# Parameters
1. NAME = This is the name of the SNMP job to be created.
2. IP_ADDRESS = A parameter necessary for job to run.
3. SNMP_STRING = A parameter necessary for job to run.
4. REMOTE_COLLECTOR_ID = A parameter necessary for job to run. If running from a clean install this will probably be '1'.
5. HOST = Endpoint of installation.
6. JOB = 'Path of api'.
7. RUN = A parameter necessary for job to run.
8. QUERY = Name of the file that has the query to get pks for a job.
9. TIMESTAMPS = Name of the file that has the query to get the end timestamps to determine if job is completed.
10. VIEW = The view to verify.
11. TEST_NAME = Test identifier.

# Folders
1. check = stores the expected views for validation.
2. config = stores column definitions.
3. query = stores doql queries.
